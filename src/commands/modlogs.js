const {
    Command
} = require('discord-akairo');
const { Embeds: EmbedsMode } = require('discord-paginationembed');
const { FieldsEmbed: FieldsEmbedMode } = require('discord-paginationembed');
const { MessageEmbed, RichEmbed } = require('discord.js');

class ModlogCommand extends Command {
    constructor() {
        super('modlogs', {
            aliases: ['modlogs', 'logs'],
            category: "utility",
            userPermissions: "BAN_MEMBERS",
            args: [{
                id: "member",
                type: "member"
            }]
        });
    }
    async exec(message, { member }) {
        if (!member) return await message.channel.send(
            new RichEmbed()
            .setTitle("I couldn't find that member")
            .setDescription("Try using their ID or a mention")
            .setColor(0xeccbd9)
        )
        var previous = await member.client.knex("warnings")
            .select()
            .where({ person_id: member.id })
            .andWhere("epoch_created", ">", Date.now() - (1000 * 60 * 60 * 24 * 7 * 2))
        previous = previous.sort((a, b) => {
            if (a.epoch_created < b.epoch_created) return 1
            if (a.epoch_created > b.epoch_created) return -1
            return 0
        })   
        if (previous.length == 0) return await message.channel.send(
            new RichEmbed()
            .setTitle(`Modlogs for ${message.author.tag}`)
            .setDescription(`Page 1 of 1`)
            .setColor(0xeccbd9)
            .setTimestamp(new Date())
            .addField(`Warnings`, `None found. Excellent dental hygiene`)
        )     
        new FieldsEmbedMode()
            .setArray(previous)
            .setAuthorizedUser(message.author)
            .setChannel(message.channel)
            .setElementsPerPage(5)
            .setPage(1)
            .showPageIndicator(true)
            .setTitle("Modlogs for " + member.user.tag)
            .setColor(0xeccbd9)
            .setTimestamp(new Date())
            .formatField('Warnings', i => `Warning ID \`${i.id}\` | Given by <@${i.moderator_id}> | Reason: \`${i.reason}\` | Date: ${new Date(i.epoch_created)}\n`)
            .build();
    }
}
module.exports = ModlogCommand;

