const {
    Command
} = require('discord-akairo');
const { Embeds: EmbedsMode } = require('discord-paginationembed');
const { FieldsEmbed: FieldsEmbedMode } = require('discord-paginationembed');
const { MessageEmbed, RichEmbed } = require('discord.js');

class ModlogCommand extends Command {
    constructor() {
        super('removewarn', {
            aliases: ['removewarn', 'delwarn'],
            category: "utility",
            userPermissions: "ADMINISTRATOR",
            args: [{
                id: "id",
                type: "string"
            }]
        });
    }
    async exec(message, { id }) {

        var previous = await member.client.knex("warnings")
            .select()
            .where()
            .andWhere("epoch_created", ">", Date.now() - (1000 * 60 * 60 * 24 * 7 * 2))
        if (previous.filter(p => p.id == id).length < 1) return await message.channel.send(
            new RichEmbed()
            .setTitle("I couldn't find that warning")
            .setDescription("Check you're using the ID from !modlogs.")
            .setColor(0xeccbd9)
        )

        await message.client.knex("warnings")
            .where({ id })
            .delete()

        return await message.channel.send(
            new RichEmbed()
                .setTitle("I cleared that warning!")
                .setDescription("*annoying drilling sounds as the dentist fixes their teeth*")
                .setColor(0xeccbd9)
        )
        
    }
}
module.exports = ModlogCommand;

