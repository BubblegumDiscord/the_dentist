const {
  Command
} = require('discord-akairo');

class EvalCommand extends Command {
  constructor() {
    super('sql', {
      aliases: ['sql'],
      split: "none",
      category: "utility",
      ownerOnly: true,
      args: [{
        id: "after"
      }]
    });
  }
  async exec(message, { after }) {
    await message.reply(JSON.stringify(await message.client.knex.raw(after)))
  }
}
module.exports = EvalCommand;

