const { Command } = require('discord-akairo');
const { RichEmbed } = require("discord.js");
const { dispenseTreatment } = require("../helpers/dispenseTreatment");

class WarnCommand extends Command {
    constructor() {
        super('warn', {
            aliases: ['warn'],
            channel: 'guild',
            clientPermissions: ['BAN_MEMBERS'],
            userPermissions: ['BAN_MEMBERS'],
            args: [
                {
                    id: 'member',
                    type: 'member'
                },{
                    id: 'reason',
                    type: 'string',
                    match: 'rest'
                }]
        });
    }

    async exec(message, { member, reason }) {
        if (reason.length < 5) return await message.channel.send(
            new RichEmbed()
            .setTitle("I can't let you do that")
            .setDescription("A reason is required to give a warning. Please give one word or more")
            .setColor(0xeccbd9)
        )
        if (!member) return await message.channel.send(
            new RichEmbed()
            .setTitle("I couldn't find that member")
            .setDescription("Try using their ID or a mention")
            .setColor(0xeccbd9)
        )

        await this.client.knex("warnings")
            .insert({
                person_id: member.id,
                moderator_id: message.member.id,
                reason: reason,
                epoch_created: Date.now()
            })
        
        await dispenseTreatment(member)
        await message.channel.send(
            new RichEmbed()
            .setTitle(":ok_hand:")
            .setDescription(`I've warned ${member}.`)
            .setColor(0xeccbd9)
        )
    }
}

module.exports = WarnCommand;