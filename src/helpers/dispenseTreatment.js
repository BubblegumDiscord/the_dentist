const { RichEmbed } = require("discord.js");

module.exports.dispenseTreatment = async (member) => {
    var previous = await member.client.knex("warnings")
      .select()
      .where({ person_id: member.id })
      .andWhere("epoch_created", ">", Date.now() - (1000 * 60 * 60 * 24 * 7 * 2))
    

    previous = previous.sort((a, b) => {
        if (a.epoch_created < b.epoch_created) return 1
        if (a.epoch_created > b.epoch_created) return -1
        return 0
    })

    var dm = await member.createDM();
    var this_warning = previous[0];

    if (previous.length == 1) {
        await dm.send(
            new RichEmbed()
                .setTitle("Your naughty behavior has earned you a visit to the dentist")
                .setDescription("**The Dentist's Diagnostic**\nLooks like you have decent oral hygiene. What a pretty smile! I'll let you off with just a warning. But if you keep it up... I'll remove all your teeth personally.")
                .setColor(0xeccbd9)
                .addBlankField()
                .addField("Warn reason", `\`${this_warning.reason}\``, true)
                .addField("Warning number", 1)
                .setTimestamp(new Date())
                .setFooter("Warn ID: " + this_warning.id)
                .setImage("http://i0.kym-cdn.com/photos/images/original/001/297/505/e62.gif")
        )
    } else if (previous.length == 2) {
        await dm.send(
            new RichEmbed()
            .setTitle("Back so soon? Amazing!")
            .setDescription("**The Dentist's Diagnostic**\nSuch poor oral maintenance. I can't wait to *fix* you up real good. Your pretty teeth, gum, enamel, blood... I want it all!")
            .setColor(0xeccbd9)
            .addBlankField()
            .addField("Warn reason", `\`${this_warning.reason}\``, true)
            .addField("Warning number", 2)
            .setTimestamp(new Date())
            .setFooter("Warn ID: " + this_warning.id)
            .setImage("https://78.media.tumblr.com/6249298bcf933e499ad1ffba017ff2df/tumblr_ox39v96Eod1v1hotuo2_640.gif")
        )
    } else if ((previous.length > 2) && (previous.length < 10)) {
        await dm.send(
            new RichEmbed()
            .setTitle("My play thing.... (You've been kicked)")
            .setDescription("**The Dentist's Diagnostic**\nIs it over so soon? There's so much more... We could have done... HNNNG")
            .setColor(0xeccbd9)
            .addBlankField()
            .addField("Warn reason", `\`${this_warning.reason}\``, true)
            .addField("Warning number", 2)
            .setTimestamp(new Date())
            .setFooter("Warn ID: " + this_warning.id)
            .setImage("https://orig00.deviantart.net/58bf/f/2017/270/e/4/himiko_gif_by_nocturnbros-dbopprf.gif")
        )
        if (!member.hasPermission("BAN_MEMBERS")) {
            await member.kick("3 dentistry warnings")
        }
    } else {
        await dm.send(
            new RichEmbed()
            .setTitle("I missed you so much... (You've been kicked)")
            .setDescription("**The Dentist's Diagnostic**\nI figured if I joined you you in the afterlife we could be together forever. You're irreplaceable.")
            .setColor(0xeccbd9)
            .addBlankField()
            .addField("Warn reason", `\`${this_warning.reason}\``, true)
            .addField("Warning number", 2)
            .setTimestamp(new Date())
            .setFooter("Warn ID: " + this_warning.id)
            .setImage("https://cdn.discordapp.com/attachments/371216051205046272/457608721325883402/ctMG9Nv.gif")
        )
        if (!member.hasPermission("BAN_MEMBERS")) {
            await member.kick("3 dentistry warnings")
        }
    }
}